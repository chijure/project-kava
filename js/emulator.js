window.addEventListener('load', function() {
      var mapping = {
        'ArrowUp': -1,
        'ArrowDown': -2,
        'ArrowLeft': -3,
        'ArrowRight': -4,
        'Enter': -5,
        'SoftLeft': -6,
        'SoftRight': -7,
        'Call': -10,
        '#': 35,
        '*': 42,
        '0': 48,
        '1': 49,
        '2': 50,
        '3': 51,
        '4': 52,
        '5': 53,
        '6': 54,
        '7': 55,
        '8': 56,
        '9': 57
      };
      window.addEventListener('keydown', function(e) {
        if (mapping[e.key]) {
          if (js2me.config.workers) {
            js2me.worker.postMessage(['sendKeyPressEvent', mapping[e.key]]);
          } else {
            js2me.sendKeyPressEvent(mapping[e.key]);
          }
        }
      });
      window.addEventListener('keyup', function(e) {
        if (mapping[e.key]) {
          if (js2me.config.workers) {
            js2me.worker.postMessage(['sendKeyReleasedEvent', mapping[e.key]]);
          } else {
            js2me.sendKeyReleasedEvent(mapping[e.key]);
          }
        } else js2me.sendKeyReleasedEvent();
      });
      var parts = location.search.substr(1).split('&');
      for (var i = 0; i < parts.length; i++) {
        var value = decodeURIComponent(parts[i].split('=')[1]);
        if (!isNaN(parseInt(value))) {
          value = parseInt(value);
        }
        js2me.config[parts[i].split('=')[0]] = value;
      }

      var screen = document.getElementById('screen');

      js2me.engine = 'js/engine.js';

      js2me.config.width = 240;
      js2me.config.height = 320 - 58;
      js2me.config.fullHeight = 320;
  
      if(js2me.config.screen === 'full') {
        document.body.classList.add('fullscreen');
        js2me.config.height = 320;
      }
      else if(js2me.config.screen === 'half') {
        document.body.classList.add('halfscreen');
        js2me.config.height = 320 - 30;
      }

      function runJAR(jarBlob) {
        document.getElementById('ok').innerHTML = '&nbsp;'
        document.getElementById('screen').innerHTML = 'Loading&hellip;'
        js2me.launchJAR(jarBlob)

      }

      if (js2me.config.src) {
        var request = new XMLHttpRequest;
        request.onload = function() {
          runJAR(request.response);
        };
        request.open('GET', js2me.config.src);
        request.responseType = 'blob';
        request.send();
      } else {
        var pickKeyHandler = function(e) {
          if(e.key === 'Enter' || e.key === 'Call') {
            var picker = new MozActivity({
              name: "xyz.831337.kava.pickFile",
              data: {}
            })
            picker.onsuccess = function() {
              var fileObj = picker.result.file
              window.removeEventListener('keydown', pickKeyHandler)
              runJAR(fileObj)
            }
          }
          else if(e.key === '#') {
            var nextMode = ''
            if(js2me.config.screen === 'full') nextMode = 'normal'
            else if(js2me.config.screen === 'half') nextMode = 'full'
            else nextMode = 'half'
            location.search = '?screen=' + nextMode
          }
        }
        window.addEventListener('keydown', pickKeyHandler)
      }
      js2me.showError = function(message) {
        window.alert(message)
      };
})